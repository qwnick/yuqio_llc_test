Главная сцена - "Assets/Scenes/Main.unity"

ИНСТРУКЦИЯ ПО МАШТАБИРОВАНИЮ


ИЗМЕНЕНИЕ РАЗМЕРОВ ПОЛЯ (FIELD)
происходит с помощью перетаскивания в инспекторе
maxXYcorner (зеленое Gizmo) и minXYcorner (красное Gizmo), 
которые указаны в Field (Field.maxXYcorner и Field.minXYcorner). 
Остальные углы (2 штуки - серый цвет) вычисляются автоматически. 
Каждый угол показывает расположение угловой карты.

ИЗМЕНЕИЕ КОЛИЧЕСТВА КАРТ НА ПОЛЕ
Field.horizontalCards - для кол-ва карт горизонтально
Field.verticalCards - для кол-ва карт вертикалько

ИЗМЕНЕНИЕ РАЗМЕРОВ КАРТЫ
Field.cardHolderSize (Vector2)

АВТОГЕНЕРАЦИЯ КУЧИ КАРТ
Скрипт CardsGenerator вызывается из меню "Tools/Generator/GenerateCardsBy_CARDS_IMAGES"
Он берет все картинки из CardsGenerator.PATH_TO_IMAGES = "Assets/CARDS_IMAGES/"
И генерирует для каждой картинки карту в CardsGenerator.PATH_TO_GENERATE = "Assets/GENERATED_CARDS/"
Для совсем новой генерации лучше удалить все сгенерированное в прошлый раз (что бы небыло путаницы с именами).
Для первого раза все готово прямо сейчас.