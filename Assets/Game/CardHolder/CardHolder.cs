﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class CardHolder : MonoBehaviour, IPointerClickHandler
{
	public BoolEvent onCardFlip;
	public UnityEvent onRemove;

	[SerializeField]
	private CardScriptableObject card;
	[SerializeField]
	private MeshRenderer cardBack;
	[SerializeField]
	private MeshRenderer cardFace;

	private Coroutine showFaceCouroutine;
	private Coroutine showBackCouroutine;

	private Field parentField;

	private static MaterialPropertyBlock propertyBlock = null;

	private bool isFaceUp = false;

	void Awake()
	{
		CardHolder.CreateStaticMaterialPropertyBlockIfNull();
	}

	private static void CreateStaticMaterialPropertyBlockIfNull()
	{
		if (CardHolder.propertyBlock == null)
		{
			CardHolder.propertyBlock = new MaterialPropertyBlock();
		}
	}
	
	public void Init(CardScriptableObject cardSource, Field parentField)
	{
		this.card = cardSource;
		this.parentField = parentField;
		SetMeshRendererTexture(cardFace, cardSource.face);
	}

	private void SetMeshRendererTexture(MeshRenderer renderer, Texture2D texture)
	{
		propertyBlock.SetTexture("_MainTex", texture);
		renderer.SetPropertyBlock(propertyBlock);
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		parentField.SelectCard(this);
	}

	public void ShowFace()
	{
		isFaceUp = true;
		onCardFlip.Invoke(isFaceUp);
	}

	public void ShowBack()
	{
		isFaceUp = false;
		onCardFlip.Invoke(isFaceUp);
	}

	public void Remove()
	{
		onRemove.Invoke();
		float secondsToDestroy = 2f;
		Destroy(this.gameObject, secondsToDestroy);
	}

	public CardScriptableObject GetCard()
	{
		return card;
	}

	public bool IsMatchWith(CardHolder other)
	{
		//can be rewritten as "return card.Equals(card);", for example
		return card.matchIdentifier == other.GetCard().matchIdentifier;
	}
}