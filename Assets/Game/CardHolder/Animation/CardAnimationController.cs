﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CardAnimationController : MonoBehaviour
{
	private Animator animator;
	[SerializeField]
	string animatorFlipPropertyName = "IsFaceUp";
	[SerializeField]
	string animatorRemovePropertyName = "Remove";
	void Awake()
	{
		animator = GetComponent<Animator>();
	}
	public void TriggerFlipAnimation(bool faceUp)
	{
		if(faceUp){
			animator.SetTrigger(animatorFlipPropertyName);
		}else{
			animator.ResetTrigger(animatorFlipPropertyName);
		}
	}
	public void TriggerRemoveAnimation(){
		Debug.Log("remove");
		animator.SetTrigger(animatorRemovePropertyName);
	}
}
