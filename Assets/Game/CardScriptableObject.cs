﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Card", menuName = "Game")]
public class CardScriptableObject : ScriptableObject {
	public Texture2D face;
	public int matchIdentifier;
}