﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ZeroZPosition : MonoBehaviour
{
#if UNITY_EDITOR
	void Update()
	{
		var clampedPosition = transform.position;
		clampedPosition.z = 0;
		transform.position = clampedPosition;
	}
#endif
}
