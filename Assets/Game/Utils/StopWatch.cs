﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StopWatch : MonoBehaviour
{
	public FloatEvent onStop;
	private float startTime = 0;
	void Start()
	{
		Reset();
	}

	public void Stop()
	{
		onStop.Invoke(Time.time - startTime);
	}

	public void Reset()
	{
		startTime = Time.time;
	}
}