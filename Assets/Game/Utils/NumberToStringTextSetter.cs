﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NumberToStringTextSetter : MonoBehaviour {

	[SerializeField]
	TextMeshProUGUI target;

	public void SetText(int numberToSet){
		target.text = numberToSet.ToString();
	}
	public void SetText(float numberToSet){
		target.text = (Mathf.RoundToInt(numberToSet).ToString() + "s");
	}
}
