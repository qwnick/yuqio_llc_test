﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

[Serializable]
public class IntEvent : UnityEvent<int> {}
[Serializable]
public class BoolEvent : UnityEvent<bool> {}
[Serializable]
public class FloatEvent : UnityEvent<float> {}
