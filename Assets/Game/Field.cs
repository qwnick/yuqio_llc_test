﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Field : MonoBehaviour
{
	public UnityEvent onFirstSelect;
	public UnityEvent onWin;


	[SerializeField] private Deck deck;
	[SerializeField] private int horizontalCards = 2;
	[SerializeField] private int verticalCards = 2;

	[SerializeField] private Vector2 cardHolderSize = new Vector2(1.4f, 2f);

	[SerializeField] private CardHolder cardHolder;

	[SerializeField] private Transform maxXYcorner;
	[SerializeField] private Transform minXYcorner;

	private List<CardHolder> cardsByPositionIndex;

	private CardHolder firstPreviousSelectedCard = null;
	private CardHolder secondPreviousSelectedCard = null;

	private CardHolder currentlySelected = null;

	private int cardsLeftOnBoard;

	private bool firstSelect = true;
	void Start()
	{
		cardsByPositionIndex = new List<CardHolder>();
		StartGame();
	}

	public void StartGame()
	{
		cardsLeftOnBoard = horizontalCards * verticalCards;
		deck.GenerateCardsInDeck();
		PlaceCardsFromDeckOnField();

		firstSelect = true;
	}

	private void PlaceCardsFromDeckOnField()
	{
		int positionIndex;
		for (positionIndex = 0; positionIndex < horizontalCards * verticalCards; positionIndex++)
		{
			if (deck.GetCardLeftAmount() > 0)
			{
				PlaceNewCardBy(positionIndex);
			}
			else
			{
				break;
			}
		}
		cardsLeftOnBoard = positionIndex;
	}

	private void PlaceNewCardBy(int positionIndex)
	{
		const float CARD_TRANSFER_TIME = 2f;
		Vector3 newCardPosition = CalculateCardPositionBy(positionIndex);
		CardHolder card = Instantiate(cardHolder, newCardPosition, Quaternion.identity);
		ApplyScale(card);
		card.Init(deck.PopCard(), this);
		if (positionIndex >= cardsByPositionIndex.Count)
		{
			cardsByPositionIndex.Add(card);
		}
		else
		{
			cardsByPositionIndex[positionIndex] = card;
		}
		StartCoroutine(LerpObjectPosition(card.transform, deck.transform.position, newCardPosition, CARD_TRANSFER_TIME));
	}
	private void ApplyScale(CardHolder card)
	{
		Vector3 scale = new Vector3(cardHolderSize.x, cardHolderSize.y, cardHolder.transform.lossyScale.z);
		card.transform.localScale = scale;
	}
	private IEnumerator LerpObjectPosition(Transform target, Vector3 startPosition, Vector3 endPosition, float transferTime)
	{
		for (float lerpPercent = 0; lerpPercent < 1; lerpPercent += Time.deltaTime / transferTime)
		{
			target.position = Vector3.Lerp(startPosition, endPosition, lerpPercent);
			yield return null;
		}
	}

	private Vector3 CalculateCardPositionBy(int placeIndex)
	{
		float fieldWidth = Mathf.Abs(maxXYcorner.position.x - minXYcorner.position.x);
		float fieldHeight = Mathf.Abs(maxXYcorner.position.y - minXYcorner.position.y);

		float cardWidth = (fieldWidth) / (horizontalCards - 1);
		float cardHeight = (fieldHeight) / (verticalCards - 1);

		var cardPosition = new Vector3(
			minXYcorner.position.x + cardWidth * (placeIndex % horizontalCards),
			minXYcorner.position.y + cardHeight * (placeIndex / horizontalCards),
			0f
		);

		return cardPosition;
	}

	public void SelectCard(CardHolder selectedCard)
	{
		if (currentlySelected == null)
		{
			HandleFirstCardSelection(selectedCard);
		}
		else if (currentlySelected == selectedCard)
		{
			HandleSameCardSelection(selectedCard);
		}
		else
		{
			HandleSecondCardSelection(selectedCard);
		}

		HandleFirstSelectEvent();
	}

	private void HandleFirstCardSelection(CardHolder selectedCard)
	{
		Debug.Log("first card selected");
		if (firstPreviousSelectedCard && selectedCard != firstPreviousSelectedCard)
		{ firstPreviousSelectedCard.ShowBack(); }
		if (secondPreviousSelectedCard && selectedCard != secondPreviousSelectedCard)
		{ secondPreviousSelectedCard.ShowBack(); }

		currentlySelected = selectedCard;
		selectedCard.ShowFace();
	}

	private void HandleSameCardSelection(CardHolder selectedCard)
	{
		Debug.Log("same card selected");
		currentlySelected.ShowBack();
		currentlySelected = null;
	}

	private void HandleSecondCardSelection(CardHolder selectedCard)
	{
		selectedCard.ShowFace();
		Debug.Log("second card selected");
		if (currentlySelected.IsMatchWith(selectedCard))
		{ HandleSecondSelectedCardMatch(selectedCard); }
		else
		{ HandleSecondSelectedCardNotMatch(selectedCard); }
	}

	private void HandleSecondSelectedCardMatch(CardHolder selectedCard)
	{

		if (!TryPlaceNewCardOnPlaceOfAnother(selectedCard)) cardsLeftOnBoard--;
		if (!TryPlaceNewCardOnPlaceOfAnother(currentlySelected)) cardsLeftOnBoard--;

		selectedCard.Remove();
		currentlySelected.Remove();

		currentlySelected = null;

		CheckWin();
	}

	private bool TryPlaceNewCardOnPlaceOfAnother(CardHolder placeSource)
	{
		if (deck.GetCardLeftAmount() > 0)
		{
			PlaceNewCardBy(cardsByPositionIndex.IndexOf(placeSource));
			return true;
		}
		else { return false; }
	}

	private void CheckWin()
	{
		if (deck.GetCardLeftAmount() == 0 && cardsLeftOnBoard == 0)
		{
			Debug.Log("win");
			onWin.Invoke();
			return;
		}
	}

	private void HandleSecondSelectedCardNotMatch(CardHolder selectedCard)
	{
		firstPreviousSelectedCard = selectedCard;
		secondPreviousSelectedCard = currentlySelected;
		currentlySelected = null;
	}

	private void HandleFirstSelectEvent()
	{
		if (firstSelect)
		{
			onFirstSelect.Invoke();
			firstSelect = false;
		}
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.green; //min corner
		Gizmos.DrawSphere(maxXYcorner.position, 0.5f);
		Gizmos.color = Color.red;//max corner
		Gizmos.DrawSphere(minXYcorner.position, 0.5f);
		Gizmos.color = Color.gray;//corners calculated from min and max
		Gizmos.DrawSphere(new Vector3(maxXYcorner.position.x, minXYcorner.position.y, 0f), 0.5f);
		Gizmos.DrawSphere(new Vector3(minXYcorner.position.x, maxXYcorner.position.y, 0f), 0.5f);
	}
}
