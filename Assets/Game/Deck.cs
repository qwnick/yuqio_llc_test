﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck : MonoBehaviour
{
	public IntEvent onCardCountChange;
	private const int MATCH_CARDS_IN_DECK = 2;
	[SerializeField]
	private List<CardScriptableObject> uniqueCards;
	[SerializeField]
	private GameObject deckBack;

	private List<CardScriptableObject> cardsInDeck;
	// Use this for initialization
	void Awake()
	{
		cardsInDeck = new List<CardScriptableObject>();
	}

	public void GenerateCardsInDeck()
	{
		cardsInDeck.Clear();
		deckBack.SetActive(true);
		foreach (CardScriptableObject card in uniqueCards)
		{
			for (int i = 0; i < MATCH_CARDS_IN_DECK; i++)
			{
				cardsInDeck.Add(card);
			}
		}
	}

	public CardScriptableObject PopCard()
	{
		int cardIndex = Random.Range(0, cardsInDeck.Count);
		var cardToPop = cardsInDeck[cardIndex];
		cardsInDeck.RemoveAt(cardIndex);
		if (cardsInDeck.Count == 0) { deckBack.SetActive(false); }

		onCardCountChange.Invoke(cardsInDeck.Count);
		return cardToPop;
	}

	public int GetCardLeftAmount()
	{
		return cardsInDeck.Count;
	}
}
