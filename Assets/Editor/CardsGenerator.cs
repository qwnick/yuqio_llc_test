using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class CardsGenerator : MonoBehaviour
{
	private const string PATH_TO_IMAGES = "Assets/CARDS_IMAGES/";
	private const string PATH_TO_GENERATE = "Assets/GENERATED_CARDS/";
	// Use this for initialization
	[MenuItem("Tools/Generator/GenerateCardsBy_CARDS_IMAGES")]
	public static void GenerateCards()
	{
		if(!AssetDatabase.IsValidFolder("Assets/GENERATED_CARDS")) AssetDatabase.CreateFolder("Assets", "GENERATED_CARDS");
		DirectoryInfo dir = new DirectoryInfo(PATH_TO_IMAGES);
		FileInfo[] filesInfo = dir.GetFiles("*.png");
		int index = 0;
		foreach (var fileInfo in filesInfo){
			var newCard = ScriptableObject.CreateInstance<CardScriptableObject>();
			newCard.face = AssetDatabase.LoadAssetAtPath<Texture2D>(PATH_TO_IMAGES+fileInfo.Name);
			
			newCard.matchIdentifier = index++;
			string path = PATH_TO_GENERATE+fileInfo.Name + ".asset";
			
			AssetDatabase.CreateAsset(newCard,path);
		}
	}
}
